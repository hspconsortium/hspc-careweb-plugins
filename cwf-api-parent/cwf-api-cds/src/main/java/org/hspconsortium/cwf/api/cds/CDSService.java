/*-
 * #%L
 * Clinical Decision Support
 * %%
 * Copyright (C) 2014 - 2016 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package org.hspconsortium.cwf.api.cds;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.carewebframework.api.event.IEventManager;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Parameters;
import org.hl7.fhir.dstu3.model.Parameters.ParametersParameterComponent;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.dstu3.model.ServiceDefinition;
import org.hl7.fhir.dstu3.model.TriggerDefinition;
import org.hl7.fhir.dstu3.model.TriggerDefinition.TriggerType;
import org.hspconsortium.cwf.fhir.common.BaseService;
import org.hspconsortium.cwf.fhir.common.FhirUtil;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.util.UrlUtil;

public class CDSService {
    
    private final BaseService fhirService;
    
    private final IGenericClient cdsClient;
    
    private final Properties eventMapping = new Properties();
    
    private volatile List<ServiceDefinition> services;
    
    public CDSService(BaseService fhirService, String fhirCDSRoot, org.springframework.core.io.Resource eventMappings)
        throws IOException {
        this.fhirService = fhirService;
        cdsClient = fhirService.getClient().getFhirContext().newRestfulGenericClient(fhirCDSRoot);
        
        if (eventMappings != null) {
            eventMapping.load(eventMappings.getInputStream());
        }
    }
    
    public List<ServiceDefinition> getServices() {
        if (services == null) {
            loadServices();
        }
        
        return Collections.unmodifiableList(services);
    }
    
    private synchronized void loadServices() {
        if (services == null) {
            Bundle bundle = cdsClient.search().forResource(ServiceDefinition.class).returnBundle(Bundle.class).execute();
            services = FhirUtil.getEntries(bundle, ServiceDefinition.class);
        }
    }
    
    public void subscribe(IEventManager eventManager, IInvocationHandler invocationHandler) {
        for (ServiceDefinition sd : getServices()) {
            for (TriggerDefinition trigger : sd.getTrigger()) {
                if (trigger.getType() == TriggerType.NAMEDEVENT) {
                    String eventName = eventMapping.getProperty(trigger.getEventName());
                    
                    if (eventName != null) {
                        eventManager.subscribe(eventName, (name, data) -> {
                            invocationHandler.invoke(sd, trigger);
                        });
                    }
                }
            }
        }
    }
    
    public Parameters evaluate(String module, Parameters params) {
        module = UrlUtil.escape(module);
        IdType operationId = new IdType("ServiceDefinition", module);
        return cdsClient.operation().onInstance(operationId).named("$evaluate").withParameters(params).encodedJson()
                .execute();
    }
    
    public Parameters evaluate(String module, Patient patient, String query) {
        query = query == null || patient == null ? query
                : StringUtils.replace(query, "{id}", patient.getIdElement().getIdPart());
        
        Bundle bundle = query == null || query.isEmpty() ? new Bundle()
                : fhirService.getClient().search().byUrl(query).returnBundle(Bundle.class).execute();
        
        return evaluate(module, patient, bundle);
    }
    
    public Parameters evaluate(String module, Patient patient, List<Resource> resources) {
        Bundle bundle = new Bundle();
        
        for (Resource resource : resources) {
            BundleEntryComponent entry = bundle.addEntry();
            entry.setResource(resource);
        }
        
        return evaluate(module, patient, bundle);
    }
    
    public Parameters evaluate(String module, Patient patient, Bundle bundle) {
        Parameters params = new Parameters();
        params.setId("ID_PARAMS");
        addParameter(params, "patient", patient);
        addParameter(params, "inputData", bundle);
        return evaluate(module, params);
    }
    
    public IGenericClient getClient() {
        return cdsClient;
    }
    
    private ParametersParameterComponent addParameter(Parameters params, String name, Resource resource) {
        if (resource != null) {
            ParametersParameterComponent param = params.addParameter();
            param.setName(name).setResource(resource);
            return param;
        }
        
        return null;
    }
}
