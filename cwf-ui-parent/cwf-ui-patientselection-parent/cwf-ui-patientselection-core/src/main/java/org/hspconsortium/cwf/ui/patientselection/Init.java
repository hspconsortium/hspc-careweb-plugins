/*
 * #%L
 * cwf-ui-patientselection-core
 * %%
 * Copyright (C) 2014 - 2016 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package org.hspconsortium.cwf.ui.patientselection;

import org.carewebframework.shell.CareWebShell;
import org.carewebframework.shell.CareWebUtil;
import org.carewebframework.shell.ICareWebStartup;
import org.carewebframework.ui.action.ActionRegistry;
import org.carewebframework.ui.action.IAction;
import org.carewebframework.ui.command.CommandUtil;
import org.fujion.event.EventUtil;
import org.fujion.event.IEventListener;
import org.hspconsortium.cwf.api.patient.PatientContext;

/**
 * Patient selection initializers.
 */
public class Init implements ICareWebStartup {

    private static final String ACTION_ID = "patientselection.select";

    private static final String ACTION_NAME = "@patientselection.action.select.label";

    private static final String FORCE_SELECT_EVENT = "force.patient.selection";

    private static final IAction PATIENT_SELECT = ActionRegistry.register(true, ACTION_ID, ACTION_NAME,
        "groovy:org.hspconsortium.cwf.ui.patientselection.PatientSelection.show(true, null);");

    private static final IEventListener forceSelectionListener = (event) -> {
        PatientSelection.show(true);
    };

    @Override
    public boolean execute() {
        CareWebShell shell = CareWebUtil.getShell();
        CommandUtil.associateCommand("PATIENT_SELECT", shell, PATIENT_SELECT);

        // call the patient selection routine at login, if the user preference is set
        if (PatientContext.getActivePatient() == null && PatientSelection.forcePatientSelection()) {
            shell.addEventListener(FORCE_SELECT_EVENT, forceSelectionListener);
            EventUtil.post(FORCE_SELECT_EVENT, shell, null);
        }

        return true;
    }

}
