/*
 * #%L
 * cwf-ui-reporting
 * %%
 * Copyright (C) 2014 - 2016 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package org.hspconsortium.cwf.ui.reporting.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.carewebframework.api.event.IGenericEvent;
import org.carewebframework.shell.elements.ElementPlugin;
import org.carewebframework.ui.dialog.DialogUtil;
import org.carewebframework.ui.sharedforms.ListFormController;
import org.carewebframework.ui.thread.ThreadEx;
import org.carewebframework.ui.thread.ThreadEx.IRunnable;
import org.carewebframework.ui.util.CWFUtil;
import org.fujion.annotation.WiredComponent;
import org.fujion.component.BaseComponent;
import org.fujion.component.Html;
import org.fujion.component.Row;
import org.fujion.component.Window;
import org.fujion.page.PageUtil;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Narrative;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.instance.model.api.IBaseDatatype;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hspconsortium.cwf.api.fhirsub.ISubscriptionCallback;
import org.hspconsortium.cwf.api.fhirsub.ResourceSubscriptionManager;
import org.hspconsortium.cwf.api.fhirsub.SubscriptionWrapper;
import org.hspconsortium.cwf.api.patient.PatientContext;
import org.hspconsortium.cwf.fhir.common.BaseService;
import org.hspconsortium.cwf.fhir.common.FhirUtil;
import org.hspconsortium.cwf.fhir.common.NarrativeService;
import org.hspconsortium.cwf.ui.reporting.Constants;

/**
 * Controller for cover sheet components.
 *
 * @param <R> Type of resource object.
 * @param <M> Type of model object.
 */
public abstract class ResourceListView<R extends IBaseResource, M> extends ListFormController<M> {

    private static final Log log = LogFactory.getLog(ResourceListView.class);

    @WiredComponent
    protected Html detailView;

    protected Patient patient;

    protected int asyncHandle;

    private String detailTitle;

    private BaseService fhirService;
    
    private NarrativeService narrativeService;
    
    private ResourceSubscriptionManager subscriptionManager;

    private final List<SubscriptionWrapper> subscriptions = new ArrayList<>();

    private String resourcePath;

    private Class<R> resourceClass;

    private final ISubscriptionCallback subscriptionListener = (eventName, resource) -> {
        refresh();
    };
    
    private final IGenericEvent<Patient> patientChangeListener = (eventName, patient) -> {
        setPatient(patient);
    };

    protected abstract void setup();

    protected void setup(Class<R> resourceClass, String title, String detailTitle, String resourcePath, int sortBy,
                         String... headers) {
        this.detailTitle = detailTitle;
        this.resourcePath = resourcePath;
        this.resourceClass = resourceClass;
        super.setup(title, sortBy, headers);
    }

    protected void createSubscriptions(Class<? extends IBaseResource> clazz) {
        createSubscription(clazz);
    }

    protected void createSubscription(Class<? extends IBaseResource> clazz) {
        String resourceName = clazz.getSimpleName();
        String id = patient.getIdElement().getIdPart();
        String criteria = resourceName + "?subject=" + id;
        SubscriptionWrapper wrapper = subscriptionManager.subscribe(criteria, subscriptionListener);
        
        if (wrapper != null) {
            subscriptions.add(wrapper);
        }
    }

    protected void removeSubscriptions() {
        for (SubscriptionWrapper wrapper : subscriptions) {
            try {
                subscriptionManager.unsubscribe(wrapper, subscriptionListener);
                subscriptions.remove(wrapper);
            } catch (Exception e) {
                log.error("Error removing resource subscription", e);
            }
        }
        
        subscriptions.clear();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Object transformData(Object data) {
        if (data instanceof IBaseDatatype) {
            return FhirUtil.getDisplayValueForType((IBaseDatatype) data);
        }

        if (data instanceof List) {
            List<?> c = (List<?>) data;

            if (c.isEmpty()) {
                return "";
            }
            
            if (c.get(0) instanceof IBaseDatatype) {
                return FhirUtil.getDisplayValueForTypes((List<IBaseDatatype>) c, ", ");
            }
        }

        return super.transformData(data);
    }

    /**
     * Override load list to clear display if no patient in context.
     */
    @Override
    protected void loadData() {
        if (patient == null) {
            asyncAbort();
            reset();
            status("No patient selected.");
        } else {
            super.loadData();
        }

        detailView.setContent(null);
    }

    @Override
    protected void requestData() {
        final String url = resourcePath.replace("#", patient.getIdElement().getIdPart());

        startBackgroundThread(new IRunnable() {

            @Override
            public void run(ThreadEx thread) throws Exception {
                Bundle bundle = fhirService.getClient().search().byUrl(url).returnBundle(Bundle.class).execute();
                thread.setAttribute("bundle", bundle);
            }

            @Override
            public void abort() {
            }

        });
    }

    @Override
    protected void threadFinished(ThreadEx thread) {
        try {
            thread.rethrow();
        } catch (Throwable e) {
            status("An unexpected error was encountered:  " + CWFUtil.formatExceptionForDisplay(e));
            return;
        }

        model.clear();
        initModel(processBundle((Bundle) thread.getAttribute("bundle")));
        renderData();
    }

    /**
     * Extracts results from the returned bundle. Override for special processing.
     *
     * @param bundle The bundle.
     * @return List of extracted resources.
     */
    protected List<R> processBundle(Bundle bundle) {
        return FhirUtil.getEntries(bundle, resourceClass);
    }

    protected abstract void initModel(List<R> entries);

    @Override
    protected void asyncAbort() {
        abortBackgroundThreads();
    }

    /**
     * Show detail for specified component.
     *
     * @param item The component containing the model object.
     */
    protected void showDetail(BaseComponent item) {
        @SuppressWarnings("unchecked")
        M modelObject = item == null ? null : (M) item.getData();
        String detail = modelObject == null ? null : getDetail(modelObject);

        if (!StringUtils.isEmpty(detail)) {
            if (getShowDetailPane()) {
                detailView.setContent(detail);
            } else {
                Map<String, Object> map = new HashMap<>();
                map.put("title", detailTitle);
                map.put("content", detail);
                map.put("allowPrint", getAllowPrint());
                try {
                    Window window = (Window) PageUtil
                            .createPage(Constants.RESOURCE_PREFIX + "resourceListDetailPopup.fsp", null, map).get(0);
                    window.modal(null);
                } catch (Exception e) {
                    DialogUtil.showError(e);
                }
            }
        }
    }

    protected String getDetail(M modelObject) {
        if (modelObject instanceof IBaseResource) {
            Narrative narrative = narrativeService.extractNarrative((IBaseResource) modelObject, true);
            return narrative == null ? null : narrative.getDivAsString();
        }

        return null;
    }

    /**
     * Display detail when row is selected.
     */
    @Override
    protected void rowSelected(Row row) {
        showDetail(row);
    }

    @Override
    public void onLoad(ElementPlugin plugin) {
        super.onLoad(plugin);
        setup();
        PatientContext.getPatientContext().addListener(patientChangeListener);
        setPatient(PatientContext.getActivePatient());
    }

    @Override
    public void onUnload() {
        PatientContext.getPatientContext().removeListener(patientChangeListener);
    }

    private void setPatient(Patient patient) {
        this.patient = patient;

        try {
            removeSubscriptions();

            if (patient != null) {
                createSubscriptions(resourceClass);
            }
        } finally {
            refresh();
        }
    }
    
    public BaseService getFhirService() {
        return fhirService;
    }

    public void setFhirService(BaseService fhirService) {
        this.fhirService = fhirService;
    }

    public NarrativeService getNarrativeService() {
        return narrativeService;
    }

    public void setNarrativeService(NarrativeService narrativeService) {
        this.narrativeService = narrativeService;
    }

    public ResourceSubscriptionManager getResourceSubscriptionManager() {
        return subscriptionManager;
    }

    public void setResourceSubscriptionManager(ResourceSubscriptionManager subscriptionManager) {
        this.subscriptionManager = subscriptionManager;
    }

}
