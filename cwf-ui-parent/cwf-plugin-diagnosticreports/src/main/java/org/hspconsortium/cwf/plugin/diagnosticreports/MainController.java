/*
 * #%L
 * cwf-ui-conditions
 * %%
 * Copyright (C) 2014 - 2016 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package org.hspconsortium.cwf.plugin.diagnosticreports;

import java.util.List;

import org.hl7.fhir.dstu3.model.DiagnosticReport;
import org.hspconsortium.cwf.ui.reporting.controller.ResourceListView;

/**
 * Controller for patient diagnostic reports display.
 */
public class MainController extends ResourceListView<DiagnosticReport, DiagnosticReport> {
    
    @Override
    protected void setup() {
        setup(DiagnosticReport.class, "Diagnostic Reports", "Report Detail", "DiagnosticReport?patient=#", 1, "Date", "Type",
            "Performed By", "Conclusion");
    }
    
    @Override
    protected void render(DiagnosticReport report, List<Object> columns) {
        columns.add(report.getEffective());
        columns.add(report.getCode());
        columns.add(!report.hasPerformer() ? null : report.getPerformerFirstRep().getActorTarget());
        columns.add(report.getConclusion());
    }
    
    @Override
    protected void initModel(List<DiagnosticReport> entries) {
        model.addAll(entries);
    }
    
}
