/*-
 * #%L
 * Scenario Configuration Plugin
 * %%
 * Copyright (C) 2014 - 2016 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package org.hspconsortium.cwf.plugin.scenario;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.carewebframework.ui.controller.FrameworkController;
import org.carewebframework.ui.dialog.DialogUtil;
import org.carewebframework.ui.util.CWFUtil;
import org.fujion.ancillary.IResponseCallback;
import org.fujion.annotation.EventHandler;
import org.fujion.annotation.WiredComponent;
import org.fujion.component.BaseComponent;
import org.fujion.component.Button;
import org.fujion.component.Cell;
import org.fujion.component.Column;
import org.fujion.component.Grid;
import org.fujion.component.Radiobutton;
import org.fujion.component.Row;
import org.fujion.component.Textbox;
import org.fujion.component.Window;
import org.fujion.model.IComponentRenderer;
import org.fujion.model.IModelAndView;
import org.fujion.model.ListModel;
import org.fujion.page.PageUtil;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hspconsortium.cwf.api.scenario.Scenario;
import org.hspconsortium.cwf.fhir.common.BaseService;
import org.hspconsortium.cwf.fhir.common.FhirUtil;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;

public class ViewResourcesController extends FrameworkController {

    private static IComponentRenderer<Row, IBaseResource> resourceRenderer = (resource) -> {
        Row row = new Row();
        row.addChild(new Cell(FhirUtil.getResourceIdPath(resource)));
        row.setData(resource);
        return row;
    };

    private static final Comparator<IBaseResource> resourceComparator = (r1, r2) -> {
        return r1.getIdElement().getValue().compareToIgnoreCase(r2.getIdElement().getValue());
    };

    @WiredComponent
    private Grid tblResources;

    @WiredComponent
    private Column colResource;

    @WiredComponent
    private Textbox txtResource;

    @WiredComponent
    private Radiobutton rbJSON;

    @WiredComponent
    private Button btnDelete;

    private Scenario scenario;

    private String title;

    private Window window;

    private final BaseService fhirService;

    private final ListModel<IBaseResource> model = new ListModel<>();

    /**
     * Display view resources dialog.
     *
     * @param scenario Scenario whose resources are to be viewed.
     * @param callback Callback upon dialog closure.
     */
    public static void show(Scenario scenario, IResponseCallback<Boolean> callback) {
        Map<String, Object> args = new HashMap<>();
        args.put("scenario", scenario);
        Window dlg = (Window) PageUtil.createPage("web/org/hspconsortium/cwf/ui/scenario/viewResources.fsp", null, args)
                .get(0);

        dlg.modal((event) -> {
            if (callback != null) {
                callback.onComplete(dlg.hasAttribute("modified"));
            }
        });
    }

    public ViewResourcesController(BaseService fhirService) {
        super();
        this.fhirService = fhirService;
    }

    @Override
    public void afterInitialized(BaseComponent comp) {
        super.afterInitialized(comp);
        window = (Window) comp;
        title = window.getTitle();
        scenario = (Scenario) comp.getAttribute("scenario");
        model.addAll(scenario.getResources());
        model.sort(resourceComparator, true);
        colResource.setSortComparator(resourceComparator);
        IModelAndView<Row, IBaseResource> mv = tblResources.getRows().getModelAndView(IBaseResource.class);
        mv.setRenderer(resourceRenderer);
        mv.setModel(model);
        updateCaption();
    }

    @EventHandler(value = "change", target = "@lboxResources")
    private void onSelect$lboxResources() {
        displayResource();
    }

    @EventHandler(value = "change", target = "@rbJSON")
    private void onChange$rbJSON() {
        displayResource();
    }

    @EventHandler(value = "click", target = "@btnDelete")
    private void onClick$btnDelete() {
        IBaseResource resource = getSelectedResource();

        DialogUtil.confirm("Delete " + FhirUtil.getResourceIdPath(resource, true) + "?", "Delete Resource", (confirm) -> {
            if (confirm) {
                try {
                    fhirService.deleteResource(resource);
                    model.remove(resource);
                    root.setAttribute("modified", true);
                    updateCaption();
                    displayResource();
                } catch (Exception e) {
                    DialogUtil.showError("Error deleting resource:\n\n" + CWFUtil.formatExceptionForDisplay(e));
                }
            }
        });
    }

    private void updateCaption() {
        window.setTitle(
            title + " - " + scenario.getName() + " (" + model.size() + " resource" + (model.size() == 1 ? ")" : "s)"));
    }

    private IBaseResource getSelectedResource() {
        Row row = tblResources.getRows().getSelectedRow();
        return row == null ? null : (IBaseResource) row.getData();
    }

    private void displayResource() {
        IBaseResource resource = getSelectedResource();

        if (resource == null) {
            txtResource.setValue(null);
            btnDelete.setDisabled(true);
        } else {
            FhirContext ctx = fhirService.getClient().getFhirContext();
            IParser parser = rbJSON.isChecked() ? ctx.newJsonParser() : ctx.newXmlParser();
            parser.setPrettyPrint(true);
            txtResource.setValue(parser.encodeResourceToString(resource));
            txtResource.selectRange(0, 0);
            btnDelete.setDisabled(false);
        }
    }

}
