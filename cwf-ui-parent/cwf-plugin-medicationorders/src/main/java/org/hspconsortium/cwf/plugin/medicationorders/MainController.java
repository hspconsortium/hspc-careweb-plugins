/*
 * #%L
 * cwf-ui-medications
 * %%
 * Copyright (C) 2014 - 2016 Healthcare Services Platform Consortium
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package org.hspconsortium.cwf.plugin.medicationorders;

import java.util.List;

import org.fujion.common.StrUtil;
import org.hl7.fhir.dstu3.model.Dosage;
import org.hl7.fhir.dstu3.model.Medication;
import org.hl7.fhir.dstu3.model.MedicationRequest;
import org.hl7.fhir.dstu3.model.Reference;
import org.hspconsortium.cwf.fhir.medication.MedicationService;
import org.hspconsortium.cwf.ui.reporting.controller.ResourceListView;

/**
 * Controller for patient conditions display.
 */
public class MainController extends ResourceListView<MedicationRequest, MedicationRequest> {
    
    private final MedicationService service;
    
    public MainController(MedicationService service) {
        this.service = service;
    }
    
    @Override
    protected void setup() {
        setup(MedicationRequest.class, "Medication Orders", "Order Detail", "MedicationRequest?patient=#", 1, "Medication",
            "Date", "Status", "Sig");
    }
    
    @Override
    protected void render(MedicationRequest script, List<Object> columns) {
        Object med = null;
        
        if (script.hasMedicationCodeableConcept()) {
            med = script.getMedication();
        } else if (script.hasMedicationReference()) {
            Medication medObject;
            medObject = getFhirService().getResource((Reference) script.getMedication(), Medication.class);
            med = medObject.getCode();
        }
        
        columns.add(med);
        columns.add(script.getAuthoredOn());
        columns.add(script.getStatus());
        columns.add(getSig(script.getDosageInstruction()));
    }
    
    private String getSig(List<Dosage> dosage) {
        StringBuilder sb = new StringBuilder();
        
        for (Dosage sig : dosage) {
            if (sb.length() > 0) {
                sb.append(StrUtil.CRLF);
            }
            
            if (sig.getText() != null) {
                sb.append(sig.getText());
            }
        }
        return sb.toString();
    }
    
    @Override
    protected void initModel(List<MedicationRequest> entries) {
        model.addAll(entries);
    }
    
}
